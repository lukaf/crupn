extern crate rand;
extern crate shellexpand;

use std::collections::HashMap;
use std::env;
use std::fs;
use std::io::Write;
use std::process;
use std::thread;
use std::time;

use serde::Deserialize;

#[derive(Debug, Deserialize)]
#[serde(default)]
struct Config {
    template: String,
    server: String,
    placeholder: String,
    configuration: String,
}

impl Default for Config {
    fn default() -> Config {
        Config {
            template: Default::default(),
            server: Default::default(),
            configuration: Default::default(),
            placeholder: "%HOST%".to_string(),
        }
    }
}

impl Config {
    pub fn new(configuration: &str, host: &str) -> Result<String, String> {
        let config = Config {
            configuration: String::from(configuration),
            server: String::from(host),
            ..Default::default()
        }
        .read_and_evaluate()?;

        config.generate()
    }

    fn generate(&self) -> Result<String, String> {
        let base = match fs::read_to_string(&self.template) {
            Ok(v) => v,
            Err(e) => {
                return Err(format!(
                    "Error reading file {}: {}",
                    &self.template.to_string(),
                    e
                ));
            }
        };

        Ok(base.replace(&self.placeholder, &self.server))
    }

    pub fn read_and_evaluate(&self) -> Result<Config, String> {
        let data = self.read_file(&self.configuration)?;
        self.evaluate(&data, &self.server)
    }

    fn evaluate(&self, data: &str, server: &str) -> Result<Config, String> {
        let parsed: HashMap<String, Config> = match serde_json::from_str(data) {
            Ok(value) => value,
            Err(err) => {
                return Err(format!("Error parsing configuration: {}", err.to_string()));
            }
        };

        let server_names = parsed
            .keys()
            .map(|x| format!("- {}", x))
            .collect::<Vec<String>>()
            .join("\n");

        let server = parsed
            .keys()
            .filter(|s| s.to_string() == server)
            .collect::<Vec<_>>();

        match server.len() {
            0 => Err(format!(
                "Server not found. Possible values:\n{}",
                server_names
            )),
            1 => {
                let template = self.expand_path(&parsed[server[0]].template)?;
                Ok(Config {
                    template: template,
                    server: parsed[server[0]].server.to_string(),
                    ..Default::default()
                })
            }
            _ => Err(format!("Unkown error")),
        }
    }

    fn expand_path(&self, file: &str) -> Result<String, String> {
        match shellexpand::full(file) {
            Ok(v) => Ok(v.to_string()),
            Err(e) => Err(e.to_string()),
        }
    }

    fn read_file(&self, file: &str) -> Result<String, String> {
        let file = self.expand_path(file)?;

        match fs::read_to_string(&file) {
            Ok(v) => Ok(v),
            Err(e) => Err(format!(
                "Error reading configuration file '{}': {}",
                file.to_string(),
                e.to_string()
            )),
        }
    }
}

fn main() {
    let args: Vec<String> = env::args().collect();
    if args.len() == 1 {
        eprintln!("Not enough arguments");
        process::exit(1);
    }

    let mut config_file = "~/.config/crupn.json";
    if args.len() == 3 {
        config_file = &args[2];
    }

    let vpn = match Config::new(config_file, &args[1]) {
        Ok(v) => v,
        Err(e) => {
            eprintln!("{}", e);
            process::exit(1);
        },
    };

    let filename = format!("/tmp/crupn-{}.ovpn", rand::random::<u32>());
    let mut f: fs::File = match fs::File::create(&filename) {
        Ok(v) => v,
        Err(e) => {
            eprintln!("{}", e);
            process::exit(1);
        }
    };

    write!(f, "{}", vpn).unwrap();

    let mut cmd = process::Command::new("sudo")
        .args(&["openvpn", &filename])
        .spawn()
        .unwrap();

    // remove configuration after a process starts (1sec)
    thread::spawn(move || {
        thread::sleep(time::Duration::from_secs(1));
        fs::remove_file(&filename).unwrap();
    });

    // just wait for child, we don't really care about the return value
    let _status = cmd.wait();
}
